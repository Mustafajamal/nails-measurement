<?php
/**
 * Plugin Name:       Nails Meaurement
 * Plugin URI:        logicsbuffer.com/
 * Description:       Custom Nails Meaurements tool for Woocommerce
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        http://logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       quote-generator
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_fancy_product' );

function custom_fancy_product() {

	// add_shortcode( 'quote_generator', 'custom_fancy_product_form_single' );
	add_action( 'wp_enqueue_scripts', 'custom_fancy_product_script');
	// add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	// add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
}

 function custom_fancy_product_script() {
		        
	// wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/custom_fancy_product/js/custom_fancy1.js',array(),time());	
	// //wp_enqueue_script( 'rt_masking_script', plugins_url().'/custom_fancy_product/js/masking.js');
	wp_enqueue_style( 'nails-css', plugins_url().'/nails-meaurement/css/nails.css',array() ,time());

	// if( is_page( 'custom-banner')){
	// wp_enqueue_style( 'rt_fancy_bootstrap', plugins_url().'/custom_fancy_product/css/bootstrap1.css');
	// }
}

// Add New menu in My acount tab
add_filter ( 'woocommerce_account_menu_items', 'misha_one_more_link' );
function misha_one_more_link( $menu_links ){
 
	// we will hook "anyuniquetext123" later
	$new = array( 'anyuniquetext123' => 'Add Meaurements' );
 
	// or in case you need 2 links
	// $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 1, true ) 
	+ $new 
	+ array_slice( $menu_links, 1, NULL, true );
 
 
	return $menu_links;
 
 
}
 
add_filter( 'woocommerce_get_endpoint_url', 'misha_hook_endpoint', 10, 4 );
function misha_hook_endpoint( $url, $endpoint, $value, $permalink ){
 
	if( $endpoint === 'anyuniquetext123' ) {
 
		// ok, here is the place for your custom URL, it could be external
		$url = site_url().'/my-account/#add_meaurement';
 
	}
	return $url;
 
}



/**
 * 1. Register new endpoint slug to use for My Account page
 */

/**
 * @important-note	Resave Permalinks or it will give 404 error
 */
function ts_custom_add_premium_support_endpoint() {
    add_rewrite_endpoint( 'premium-support', EP_ROOT | EP_PAGES );
}
  
add_action( 'init', 'ts_custom_add_premium_support_endpoint' );
  
  
/**
 * 2. Add new query var
 */
  
function ts_custom_premium_support_query_vars( $vars ) {
    $vars[] = 'premium-support';
    return $vars;
}
  
add_filter( 'woocommerce_get_query_vars', 'ts_custom_premium_support_query_vars', 0 );
  
  
/**
 * 3. Insert the new endpoint into the My Account menu
 */
  
function ts_custom_add_premium_support_link_my_account( $items ) {
    $items['premium-support'] = 'Edit Meaurements';
    return $items;
}
  
add_filter( 'woocommerce_account_menu_items', 'ts_custom_add_premium_support_link_my_account' );
  
  
/**
 * 4. Add content to the new endpoint
 */
  
function ts_custom_premium_support_content() {
	echo '<h3>Premium WooCommerce Support</h3><p>Welcome to the WooCommerce support area. As a premium customer, you can submit a ticket should you have any WooCommerce issues with your website, snippets or customization. <i>Please contact your theme/plugin developer for theme/plugin-related support.</i></p>';
	
	echo do_shortcode( ' /* your shortcode here */ ' );
	acf_form_head();

	?><div id="primary" class="content-area">
	    <div id="content" class="site-content" role="main">
	    <?php while ( have_posts() ) : the_post(); ?>
	    <?php acf_form(array(
	        'post_id'       => 123,
	        'post_title'    => false,
	        'post_content'  => false,
	        'submit_value'  => __('Update meta')
	    )); ?>
	    <?php endwhile; ?>
	    </div><!-- #content -->
	</div><!-- #primary --><?php
 
}

/**
 * @important-note	"add_action" must follow 'woocommerce_account_{your-endpoint-slug}_endpoint' format
 */
add_action( 'woocommerce_account_premium-support_endpoint', 'ts_custom_premium_support_content' );




add_action('pre_get_posts', 'filter_posts_list');
function filter_posts_list($query)
{
    //$pagenow holds the name of the current page being viewed
     global $pagenow;
 
    //$current_user uses the get_currentuserinfo() method to get the currently logged in user's data
     global $current_user;
     get_currentuserinfo();
     
     //Shouldn't happen for the admin, but for any role with the edit_posts capability and only on the posts list page, that is edit.php
      if(!current_user_can('administrator') && current_user_can('edit_posts') && ('edit.php' == $pagenow)){
        //global $query's set() method for setting the author as the current user's id
        $query->set('author', $current_user->ID);
        }
}
// Show only posts and media related to logged in author
// add_action('pre_get_posts', 'query_set_only_author' );
// function query_set_only_author( $wp_query ) {
//     global $current_user;
//     if( is_admin() && !current_user_can('edit_others_posts') ) {
//         $wp_query->set( 'author', $current_user->ID );
//         add_filter('views_edit-post', 'fix_post_counts');
//         add_filter('views_upload', 'fix_media_counts');
//     }
// }
// Fix post counts
// function fix_post_counts($views) {
//     global $current_user, $wp_query;
//     unset($views['mine']);
//     $types = array(
//         array( 'status' =>  NULL ),
//         array( 'status' => 'publish' ),
//         array( 'status' => 'draft' ),
//         array( 'status' => 'pending' ),
//         array( 'status' => 'trash' )
//     );
//     foreach( $types as $type ) {
//         $query = array(
//             'author'      => $current_user->ID,
//             'post_type'   => 'post',
//             'post_status' => $type['status']
//         );
//         $result = new WP_Query($query);
//         if( $type['status'] == NULL ):
//             $class = ($wp_query->query_vars['post_status'] == NULL) ? ' class="current"' : '';
//             $views['all'] = sprintf(__('<a href="%s"'. $class .'>All <span class="count">(%d)</span></a>', 'all'),
//                 admin_url('edit.php?post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'publish' ):
//             $class = ($wp_query->query_vars['post_status'] == 'publish') ? ' class="current"' : '';
//             $views['publish'] = sprintf(__('<a href="%s"'. $class .'>Published <span class="count">(%d)</span></a>', 'publish'),
//                 admin_url('edit.php?post_status=publish&post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'draft' ):
//             $class = ($wp_query->query_vars['post_status'] == 'draft') ? ' class="current"' : '';
//             $views['draft'] = sprintf(__('<a href="%s"'. $class .'>Draft'. ((sizeof($result->posts) > 1) ? "s" : "") .' <span class="count">(%d)</span></a>', 'draft'),
//                 admin_url('edit.php?post_status=draft&post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'pending' ):
//             $class = ($wp_query->query_vars['post_status'] == 'pending') ? ' class="current"' : '';
//             $views['pending'] = sprintf(__('<a href="%s"'. $class .'>Pending <span class="count">(%d)</span></a>', 'pending'),
//                 admin_url('edit.php?post_status=pending&post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'trash' ):
//             $class = ($wp_query->query_vars['post_status'] == 'trash') ? ' class="current"' : '';
//             $views['trash'] = sprintf(__('<a href="%s"'. $class .'>Trash <span class="count">(%d)</span></a>', 'trash'),
//                 admin_url('edit.php?post_status=trash&post_type=post'),
//                 $result->found_posts);
//         endif;
//     }
//     return $views;
// }

// define the woocommerce_before_variations_form callback 
function action_woocommerce_before_variations_form(  ) { 
	$userID;
	if ( wp_get_current_user() instanceof WP_User ) {
	    $userID = wp_get_current_user()->ID;
	}
	/**
	 * Setup query to show the ‘services’ post type with all posts filtered by 'home' category.
	 * Output is linked title with featured image and excerpt.
	 */
	$args = array(  
	    'post_type' => 'meaurement',
	    'post_status' => 'publish',
	    'posts_per_page' => -1, 
	    'orderby' => 'title', 
	    'order' => 'ASC',
	    'author' => $userID,
	);

	$loop = new WP_Query( $args ); 
	
	?><select name="meaurements" id="meaurements"><?php    
	while ( $loop->have_posts() ) : $loop->the_post(); 
	    //$featured_img = wp_get_attachment_image_src( $post->ID );
	    $meaurement_title =  get_the_title();
	    $meaurement_id =  get_the_ID();
	    $meaurement_url =  get_the_permalink();
	    //echo $meaurement_id;
	    //$stiletto_l_pinky = get_field( 'stiletto-l-pinky', $meaurement_id );
	    ?>
	    <option id="<?php echo $meaurement_id; ?>" meaurement-url="<?php echo $meaurement_url; ?>" value="<?php echo $meaurement_title ?>"><?php echo $meaurement_title; ?></option><?php
	endwhile;
	?></select>
	<script type="text/javascript">
    	jQuery( document ).ready(function() {
    		//var option = jQuery('meaurements:selected', this).attr('data-val-pinky');
    		jQuery("#meaurements").change(function(){ 
		        //Left Hand Stiletto
		        //Pinky
		          //Short
		        var meaurement_url_o = jQuery(this).find('option:selected'); 
		        var meaurement_url = meaurement_url_o.attr("meaurement-url"); 
		        console.log("meaurement_url..."+meaurement_url);
		        jQuery('#meaurement-url').val(meaurement_url);
		        		
		    }); 
    	});
    </script>

	<?php
	wp_reset_postdata(); 
}; 
         
// add the action 
add_action( 'woocommerce_before_add_to_cart_form', 'action_woocommerce_before_variations_form', 10, 0 ); 


/**
 * Output engraving field.
*/
function iconic_output_engraving_field() {
    global $product;
    //if ( $product->get_id() !== 1741 ) {
    //    return;
    //}
    ?>
    
    <div class="elementor-container elementor-column-gap-no nails-meaurement-single-product">
        
    	<div class="elementor-row">
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Get the selected Meaurement -->
				<input type="text" id="meaurement-url" name="meaurement-url">
				
			</div>
		</div>
        
    </div>
    <?php
}
add_action( 'woocommerce_before_add_to_cart_button', 'iconic_output_engraving_field', 10 );

/**
 * Add engraving text to cart item.
 * @param array $cart_item_data
 * @param int   $product_id
 * @param int   $variation_id
 * @return array
 */
function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
    $meaurement_url = filter_input( INPUT_POST, 'meaurement-url' );
    if ( empty( $meaurement_url ) ) {
        return $cart_item_data;
    }
    $cart_item_data['meaurement-url'] = $meaurement_url;
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );

/**
 * Display engraving text in the cart.
 *
 * @param array $item_data
 * @param array $cart_item
 *
 * @return array
 */
function iconic_display_engraving_text_cart( $item_data, $cart_item ) {
    if ( empty( $cart_item['meaurement-url'] ) ) {
        return $item_data;
    }
    $item_data[] = array(
        'key'     => __( 'Meaurement', 'nails' ),
        'value'   => wc_clean( $cart_item['meaurement-url'] ),
        'display' => '',
    );
    return $item_data;
}
add_filter( 'woocommerce_get_item_data', 'iconic_display_engraving_text_cart', 10, 2 );

/**
 * Add engraving text to order.
 *
 * @param WC_Order_Item_Product $item
 * @param string                $cart_item_key
 * @param array                 $values
 * @param WC_Order              $order
 */

function iconic_add_engraving_text_to_order_items( $item, $cart_item_key, $values, $order ) {
    if ( empty( $values['meaurement-url'] ) ) {
        return;
    }
    $item->add_meta_data( __( 'Measurement', 'nails' ), $values['meaurement-url'] );
}
add_action( 'woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4 );
