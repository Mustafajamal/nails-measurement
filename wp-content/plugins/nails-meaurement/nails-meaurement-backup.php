<?php
/**
 * Plugin Name:       Nails Meaurement
 * Plugin URI:        logicsbuffer.com/
 * Description:       Custom Nails Meaurements tool for Woocommerce
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        http://logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       quote-generator
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_fancy_product' );

function custom_fancy_product() {

	// add_shortcode( 'quote_generator', 'custom_fancy_product_form_single' );
	add_action( 'wp_enqueue_scripts', 'custom_fancy_product_script');
	// add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	// add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
}

 function custom_fancy_product_script() {
		        
	// wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/custom_fancy_product/js/custom_fancy1.js',array(),time());	
	// //wp_enqueue_script( 'rt_masking_script', plugins_url().'/custom_fancy_product/js/masking.js');
	wp_enqueue_style( 'nails-css', plugins_url().'/nails-meaurement/css/nails.css',array() ,time());

	// if( is_page( 'custom-banner')){
	// wp_enqueue_style( 'rt_fancy_bootstrap', plugins_url().'/custom_fancy_product/css/bootstrap1.css');
	// }
}

// Add New menu in My acount tab
add_filter ( 'woocommerce_account_menu_items', 'misha_one_more_link' );
function misha_one_more_link( $menu_links ){
 
	// we will hook "anyuniquetext123" later
	$new = array( 'anyuniquetext123' => 'Add Meaurements' );
 
	// or in case you need 2 links
	// $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 1, true ) 
	+ $new 
	+ array_slice( $menu_links, 1, NULL, true );
 
 
	return $menu_links;
 
 
}
 
add_filter( 'woocommerce_get_endpoint_url', 'misha_hook_endpoint', 10, 4 );
function misha_hook_endpoint( $url, $endpoint, $value, $permalink ){
 
	if( $endpoint === 'anyuniquetext123' ) {
 
		// ok, here is the place for your custom URL, it could be external
		$url = site_url().'/my-account/#add_meaurement';
 
	}
	return $url;
 
}


add_action('pre_get_posts', 'filter_posts_list');
function filter_posts_list($query)
{
    //$pagenow holds the name of the current page being viewed
     global $pagenow;
 
    //$current_user uses the get_currentuserinfo() method to get the currently logged in user's data
     global $current_user;
     get_currentuserinfo();
     
     //Shouldn't happen for the admin, but for any role with the edit_posts capability and only on the posts list page, that is edit.php
      if(!current_user_can('administrator') && current_user_can('edit_posts') && ('edit.php' == $pagenow)){
        //global $query's set() method for setting the author as the current user's id
        $query->set('author', $current_user->ID);
        }
}
// Show only posts and media related to logged in author
// add_action('pre_get_posts', 'query_set_only_author' );
// function query_set_only_author( $wp_query ) {
//     global $current_user;
//     if( is_admin() && !current_user_can('edit_others_posts') ) {
//         $wp_query->set( 'author', $current_user->ID );
//         add_filter('views_edit-post', 'fix_post_counts');
//         add_filter('views_upload', 'fix_media_counts');
//     }
// }
// Fix post counts
// function fix_post_counts($views) {
//     global $current_user, $wp_query;
//     unset($views['mine']);
//     $types = array(
//         array( 'status' =>  NULL ),
//         array( 'status' => 'publish' ),
//         array( 'status' => 'draft' ),
//         array( 'status' => 'pending' ),
//         array( 'status' => 'trash' )
//     );
//     foreach( $types as $type ) {
//         $query = array(
//             'author'      => $current_user->ID,
//             'post_type'   => 'post',
//             'post_status' => $type['status']
//         );
//         $result = new WP_Query($query);
//         if( $type['status'] == NULL ):
//             $class = ($wp_query->query_vars['post_status'] == NULL) ? ' class="current"' : '';
//             $views['all'] = sprintf(__('<a href="%s"'. $class .'>All <span class="count">(%d)</span></a>', 'all'),
//                 admin_url('edit.php?post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'publish' ):
//             $class = ($wp_query->query_vars['post_status'] == 'publish') ? ' class="current"' : '';
//             $views['publish'] = sprintf(__('<a href="%s"'. $class .'>Published <span class="count">(%d)</span></a>', 'publish'),
//                 admin_url('edit.php?post_status=publish&post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'draft' ):
//             $class = ($wp_query->query_vars['post_status'] == 'draft') ? ' class="current"' : '';
//             $views['draft'] = sprintf(__('<a href="%s"'. $class .'>Draft'. ((sizeof($result->posts) > 1) ? "s" : "") .' <span class="count">(%d)</span></a>', 'draft'),
//                 admin_url('edit.php?post_status=draft&post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'pending' ):
//             $class = ($wp_query->query_vars['post_status'] == 'pending') ? ' class="current"' : '';
//             $views['pending'] = sprintf(__('<a href="%s"'. $class .'>Pending <span class="count">(%d)</span></a>', 'pending'),
//                 admin_url('edit.php?post_status=pending&post_type=post'),
//                 $result->found_posts);
//         elseif( $type['status'] == 'trash' ):
//             $class = ($wp_query->query_vars['post_status'] == 'trash') ? ' class="current"' : '';
//             $views['trash'] = sprintf(__('<a href="%s"'. $class .'>Trash <span class="count">(%d)</span></a>', 'trash'),
//                 admin_url('edit.php?post_status=trash&post_type=post'),
//                 $result->found_posts);
//         endif;
//     }
//     return $views;
// }

// define the woocommerce_before_variations_form callback 
function action_woocommerce_before_variations_form(  ) { 
	$userID;
	if ( wp_get_current_user() instanceof WP_User ) {
	    $userID = wp_get_current_user()->ID;
	}
	/**
	 * Setup query to show the ‘services’ post type with all posts filtered by 'home' category.
	 * Output is linked title with featured image and excerpt.
	 */
	$args = array(  
	    'post_type' => 'meaurement',
	    'post_status' => 'publish',
	    'posts_per_page' => -1, 
	    'orderby' => 'title', 
	    'order' => 'ASC',
	    'author' => $userID,
	);

	$loop = new WP_Query( $args ); 
	
	?><select name="meaurements" id="meaurements"><?php    
	while ( $loop->have_posts() ) : $loop->the_post(); 
	    //$featured_img = wp_get_attachment_image_src( $post->ID );
	    $meaurement_title =  get_the_title();
	    $meaurement_id =  get_the_ID();
	    //echo $meaurement_id;
	    

	    //Left Hand Steletto
	    //Pinky
	    $stiletto_l_pinky = get_field( 'stiletto-l-pinky', $meaurement_id );
	    //Ring
	    $stiletto_l_ring = get_field( 'stiletto-l-ring', $meaurement_id );
	    //Middle
	    $stiletto_l_middle = get_field( 'stiletto-l-middle', $meaurement_id );
	    //Index
	    $stiletto_l_index = get_field( 'stiletto-l-index', $meaurement_id );
	    //Thumb
	    $stiletto_l_thumb = get_field( 'stiletto-l-thumb', $meaurement_id );

	    //Right Hand Steletto
	    //Pinky
	    $stiletto_r_pinky = get_field( 'stiletto-r-pinky', $meaurement_id );
	    //Ring
	    $stiletto_r_ring = get_field( 'stiletto-r-ring', $meaurement_id );
	    //Middle
	    $stiletto_r_middle = get_field( 'stiletto-r-middle', $meaurement_id );
	    //Index
	    $stiletto_r_index = get_field( 'stiletto-r-index', $meaurement_id );
	    //Thumb
	    $stiletto_r_thumb = get_field( 'stiletto-r-thumb', $meaurement_id );
	    
		//Left Hand Oval
		//Pinky
		$oval_l_pinky = get_field( 'oval-l-pinky', $meaurement_id );
		//Ring
		$oval_l_ring = get_field( 'oval-l-ring', $meaurement_id );
		//Middle
		$oval_l_middle = get_field( 'oval-l-middle', $meaurement_id );
		//Index
		$oval_l_index = get_field( 'oval-l-index', $meaurement_id );
		//Thumb
		$oval_l_thumb = get_field( 'oval-l-thumb', $meaurement_id );

		//Right Hand Oval
		//Pinky
		$oval_r_pinky = get_field( 'oval-r-pinky', $meaurement_id );
		//Ring
		$oval_r_ring = get_field( 'oval-r-ring', $meaurement_id );
		//Middle
		$oval_r_middle = get_field( 'oval-r-middle', $meaurement_id );
		//Index
		$oval_r_index = get_field( 'oval-r-index', $meaurement_id );
		//Thumb
		$oval_r_thumb = get_field( 'oval-r-thumb', $meaurement_id ); 
		
		//Left Hand Coffin
		//Pinky
		$coffin_l_pinky = get_field( 'coffin-l-pinky', $meaurement_id );
		//Ring
		$coffin_l_ring = get_field( 'coffin-l-ring', $meaurement_id );
		//Middle
		$coffin_l_middle = get_field( 'coffin-l-middle', $meaurement_id );
		//Index
		$coffin_l_index = get_field( 'coffin-l-index', $meaurement_id );
		//Thumb
		$coffin_l_thumb = get_field( 'coffin-l-thumb', $meaurement_id );

		//Right Hand Coffin
		//Pinky
		$coffin_r_pinky = get_field( 'coffin-r-pinky', $meaurement_id );
		//Ring
		$coffin_r_ring = get_field( 'coffin-r-ring', $meaurement_id );
		//Middle
		$coffin_r_middle = get_field( 'coffin-r-middle', $meaurement_id );
		//Index
		$coffin_r_index = get_field( 'coffin-r-index', $meaurement_id );
		//Thumb
		$coffin_r_thumb = get_field( 'coffin-r-thumb', $meaurement_id );
	
		//Left Hand Square
		//Pinky
		$square_l_pinky = get_field( 'square-l-pinky', $meaurement_id );
		//Ring
		$square_l_ring = get_field( 'square-l-ring', $meaurement_id );
		//Middle
		$square_l_middle = get_field( 'square-l-middle', $meaurement_id );
		//Index
		$square_l_index = get_field( 'square-l-index', $meaurement_id );
		//Thumb
		$square_l_thumb = get_field( 'square-l-thumb', $meaurement_id );

		//Right Hand Square
		//Pinky
		$square_r_pinky = get_field( 'square-r-pinky', $meaurement_id );
		//Ring
		$square_r_ring = get_field( 'square-r-ring', $meaurement_id );
		//Middle
		$square_r_middle = get_field( 'square-r-middle', $meaurement_id );
		//Index
		$square_r_index = get_field( 'square-r-index', $meaurement_id );
		//Thumb
		$square_r_thumb = get_field( 'square-r-thumb', $meaurement_id );
	    ?>
	    <option id="<?php echo $meaurement_id; ?>" stiletto_r_pinky_short="<?php echo $stiletto_r_pinky['stiletto-r-pinky-short'];?>" stiletto_r_pinky_medium="<?php echo $stiletto_r_pinky['stiletto-r-pinky-medium'];?>" stiletto_r_pinky_large="<?php echo $stiletto_r_pinky['stiletto-r-pinky-large'];?>" stiletto_r_ring_short="<?php echo $stiletto_r_ring['stiletto-r-ring-short'];?>" stiletto_r_ring_medium="<?php echo $stiletto_r_ring['stiletto-r-ring-medium'];?>" stiletto_r_ring_large="<?php echo $stiletto_r_ring['stiletto-r-ring-large'];?>" stiletto_r_middle_short="<?php echo $stiletto_r_middle['stiletto-r-middle-short'];?>" stiletto_r_middle_medium="<?php echo $stiletto_r_middle['stiletto-r-middle-medium'];?>" stiletto_r_middle_large="<?php echo $stiletto_r_middle['stiletto-r-middle-large'];?>" stiletto_r_index_short="<?php echo $stiletto_r_index['stiletto-r-index-short'];?>" stiletto_r_index_medium="<?php echo $stiletto_r_index['stiletto-r-index-medium'];?>" stiletto_r_index_large="<?php echo $stiletto_r_index['stiletto-r-index-large'];?>" stiletto_r_thumb_short="<?php echo $stiletto_r_thumb['stiletto-r-thumb-short'];?>" stiletto_r_thumb_medium="<?php echo $stiletto_r_thumb['stiletto-r-thumb-medium'];?>" stiletto_r_thumb_large="<?php echo $stiletto_r_thumb['stiletto-r-thumb-large'];?>" stiletto_l_pinky_short="<?php echo $stiletto_l_pinky['stiletto-l-pinky-short'];?>" stiletto_l_pinky_medium="<?php echo $stiletto_l_pinky['stiletto-l-pinky-medium'];?>" stiletto_l_pinky_large="<?php echo $stiletto_l_pinky['stiletto-l-pinky-large'];?>" stiletto_l_ring_short="<?php echo $stiletto_l_ring['stiletto-l-ring-short'];?>" stiletto_l_ring_medium="<?php echo $stiletto_l_ring['stiletto-l-ring-medium'];?>" stiletto_l_ring_large="<?php echo $stiletto_l_ring['stiletto-l-ring-large'];?>" stiletto_l_middle_short="<?php echo $stiletto_l_middle['stiletto-l-middle-short'];?>" stiletto_l_middle_medium="<?php echo $stiletto_l_middle['stiletto-l-middle-medium'];?>" stiletto_l_middle_large="<?php echo $stiletto_l_middle['stiletto-l-middle-large'];?>" stiletto_l_index_short="<?php echo $stiletto_l_index['stiletto-l-index-short'];?>" stiletto_l_index_medium="<?php echo $stiletto_l_index['stiletto-l-index-medium'];?>" stiletto_l_index_large="<?php echo $stiletto_l_index['stiletto-l-index-large'];?>" stiletto_l_thumb_short="<?php echo $stiletto_l_thumb['stiletto-l-thumb-short'];?>" stiletto_l_thumb_medium="<?php echo $stiletto_l_thumb['stiletto-l-thumb-medium'];?>" stiletto_l_thumb_large="<?php echo $stiletto_l_thumb['stiletto-l-thumb-large'];?>" oval_r_pinky_short="<?php echo $oval_r_pinky['oval-r-pinky-short'];?>" oval_r_pinky_medium="<?php echo $oval_r_pinky['oval-r-pinky-medium'];?>" oval_r_pinky_large="<?php echo $oval_r_pinky['oval-r-pinky-large'];?>" oval_r_ring_short="<?php echo $oval_r_ring['oval-r-ring-short'];?>" oval_r_ring_medium="<?php echo $oval_r_ring['oval-r-ring-medium'];?>" oval_r_ring_large="<?php echo $oval_r_ring['oval-r-ring-large'];?>" oval_r_middle_short="<?php echo $oval_r_middle['oval-r-middle-short'];?>" oval_r_middle_medium="<?php echo $oval_r_middle['oval-r-middle-medium'];?>" oval_r_middle_large="<?php echo $oval_r_middle['oval-r-middle-large'];?>" oval_r_index_short="<?php echo $oval_r_index['oval-r-index-short'];?>" oval_r_index_medium="<?php echo $oval_r_index['oval-r-index-medium'];?>" oval_r_index_large="<?php echo $oval_r_index['oval-r-index-large'];?>" oval_r_thumb_short="<?php echo $oval_r_thumb['oval-r-thumb-short'];?>" oval_r_thumb_medium="<?php echo $oval_r_thumb['oval-r-thumb-medium'];?>" oval_r_thumb_large="<?php echo $oval_r_thumb['oval-r-thumb-large'];?>" oval_l_pinky_short="<?php echo $oval_l_pinky['oval-l-pinky-short'];?>" oval_l_pinky_medium="<?php echo $oval_l_pinky['oval-l-pinky-medium'];?>" oval_l_pinky_large="<?php echo $oval_l_pinky['oval-l-pinky-large'];?>" oval_l_ring_short="<?php echo $oval_l_ring['oval-l-ring-short'];?>" oval_l_ring_medium="<?php echo $oval_l_ring['oval-l-ring-medium'];?>" oval_l_ring_large="<?php echo $oval_l_ring['oval-l-ring-large'];?>" oval_l_middle_short="<?php echo $oval_l_middle['oval-l-middle-short'];?>" oval_l_middle_medium="<?php echo $oval_l_middle['oval-l-middle-medium'];?>" oval_l_middle_large="<?php echo $oval_l_middle['oval-l-middle-large'];?>" oval_l_index_short="<?php echo $oval_l_index['oval-l-index-short'];?>" oval_l_index_medium="<?php echo $oval_l_index['oval-l-index-medium'];?>" oval_l_index_large="<?php echo $oval_l_index['oval-l-index-large'];?>" oval_l_thumb_short="<?php echo $oval_l_thumb['oval-l-thumb-short'];?>" oval_l_thumb_medium="<?php echo $oval_l_thumb['oval-l-thumb-medium'];?>" oval_l_thumb_large="<?php echo $oval_l_thumb['oval-l-thumb-large'];?>" coffin_r_pinky_short="<?php echo $coffin_r_pinky['coffin-r-pinky-short'];?>" coffin_r_pinky_medium="<?php echo $coffin_r_pinky['coffin-r-pinky-medium'];?>" coffin_r_pinky_large="<?php echo $coffin_r_pinky['coffin-r-pinky-large'];?>" coffin_r_ring_short="<?php echo $coffin_r_ring['coffin-r-ring-short'];?>" coffin_r_ring_medium="<?php echo $coffin_r_ring['coffin-r-ring-medium'];?>" coffin_r_ring_large="<?php echo $coffin_r_ring['coffin-r-ring-large'];?>" coffin_r_middle_short="<?php echo $coffin_r_middle['coffin-r-middle-short'];?>" coffin_r_middle_medium="<?php echo $coffin_r_middle['coffin-r-middle-medium'];?>" coffin_r_middle_large="<?php echo $coffin_r_middle['coffin-r-middle-large'];?>" coffin_r_index_short="<?php echo $coffin_r_index['coffin-r-index-short'];?>" coffin_r_index_medium="<?php echo $coffin_r_index['coffin-r-index-medium'];?>" coffin_r_index_large="<?php echo $coffin_r_index['coffin-r-index-large'];?>" coffin_r_thumb_short="<?php echo $coffin_r_thumb['coffin-r-thumb-short'];?>" coffin_r_thumb_medium="<?php echo $coffin_r_thumb['coffin-r-thumb-medium'];?>" coffin_r_thumb_large="<?php echo $coffin_r_thumb['coffin-r-thumb-large'];?>" coffin_l_pinky_short="<?php echo $coffin_l_pinky['coffin-l-pinky-short'];?>" coffin_l_pinky_medium="<?php echo $coffin_l_pinky['coffin-l-pinky-medium'];?>" coffin_l_pinky_large="<?php echo $coffin_l_pinky['coffin-l-pinky-large'];?>" coffin_l_ring_short="<?php echo $coffin_l_ring['coffin-l-ring-short'];?>" coffin_l_ring_medium="<?php echo $coffin_l_ring['coffin-l-ring-medium'];?>" coffin_l_ring_large="<?php echo $coffin_l_ring['coffin-l-ring-large'];?>" coffin_l_middle_short="<?php echo $coffin_l_middle['coffin-l-middle-short'];?>" coffin_l_middle_medium="<?php echo $coffin_l_middle['coffin-l-middle-medium'];?>" coffin_l_middle_large="<?php echo $coffin_l_middle['coffin-l-middle-large'];?>" coffin_l_index_short="<?php echo $coffin_l_index['coffin-l-index-short'];?>" coffin_l_index_medium="<?php echo $coffin_l_index['coffin-l-index-medium'];?>" coffin_l_index_large="<?php echo $coffin_l_index['coffin-l-index-large'];?>" coffin_l_thumb_short="<?php echo $coffin_l_thumb['coffin-l-thumb-short'];?>" coffin_l_thumb_medium="<?php echo $coffin_l_thumb['coffin-l-thumb-medium'];?>" coffin_l_thumb_large="<?php echo $coffin_l_thumb['coffin-l-thumb-large'];?>" square_r_pinky_short="<?php echo $square_r_pinky['square-r-pinky-short'];?>" square_r_pinky_medium="<?php echo $square_r_pinky['square-r-pinky-medium'];?>" square_r_pinky_large="<?php echo $square_r_pinky['square-r-pinky-large'];?>" square_r_ring_short="<?php echo $square_r_ring['square-r-ring-short'];?>" square_r_ring_medium="<?php echo $square_r_ring['square-r-ring-medium'];?>" square_r_ring_large="<?php echo $square_r_ring['square-r-ring-large'];?>" square_r_middle_short="<?php echo $square_r_middle['square-r-middle-short'];?>" square_r_middle_medium="<?php echo $square_r_middle['square-r-middle-medium'];?>" square_r_middle_large="<?php echo $square_r_middle['square-r-middle-large'];?>" square_r_index_short="<?php echo $square_r_index['square-r-index-short'];?>" square_r_index_medium="<?php echo $square_r_index['square-r-index-medium'];?>" square_r_index_large="<?php echo $square_r_index['square-r-index-large'];?>" square_r_thumb_short="<?php echo $square_r_thumb['square-r-thumb-short'];?>" square_r_thumb_medium="<?php echo $square_r_thumb['square-r-thumb-medium'];?>" square_r_thumb_large="<?php echo $square_r_thumb['square-r-thumb-large'];?>" square_l_pinky_short="<?php echo $square_l_pinky['square-l-pinky-short'];?>" square_l_pinky_medium="<?php echo $square_l_pinky['square-l-pinky-medium'];?>" square_l_pinky_large="<?php echo $square_l_pinky['square-l-pinky-large'];?>" square_l_ring_short="<?php echo $square_l_ring['square-l-ring-short'];?>" square_l_ring_medium="<?php echo $square_l_ring['square-l-ring-medium'];?>" square_l_ring_large="<?php echo $square_l_ring['square-l-ring-large'];?>" square_l_middle_short="<?php echo $square_l_middle['square-l-middle-short'];?>" square_l_middle_medium="<?php echo $square_l_middle['square-l-middle-medium'];?>" square_l_middle_large="<?php echo $square_l_middle['square-l-middle-large'];?>" square_l_index_short="<?php echo $square_l_index['square-l-index-short'];?>" square_l_index_medium="<?php echo $square_l_index['square-l-index-medium'];?>" square_l_index_large="<?php echo $square_l_index['square-l-index-large'];?>" square_l_thumb_short="<?php echo $square_l_thumb['square-l-thumb-short'];?>" square_l_thumb_medium="<?php echo $square_l_thumb['square-l-thumb-medium'];?>" square_l_thumb_large="<?php echo $square_l_thumb['square-l-thumb-large'];?>" value="<?php echo $meaurement_title ?>"><?php echo $meaurement_title; ?></option><?php
	endwhile;
	?></select><?php
	wp_reset_postdata(); 
}; 
         
// add the action 
add_action( 'woocommerce_before_add_to_cart_form', 'action_woocommerce_before_variations_form', 10, 0 ); 


/**
 * Output engraving field.
*/
function iconic_output_engraving_field() {
    global $product;
    //if ( $product->get_id() !== 1741 ) {
    //    return;
    //}
    ?>
    <script type="text/javascript">
    	jQuery( document ).ready(function() {
    		//var option = jQuery('meaurements:selected', this).attr('data-val-pinky');
    		jQuery("#meaurements").change(function(){ 
		        //Left Hand Stiletto
		        //Pinky
		          //Short
		        var stiletto_l_pinky_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_pinky_short = stiletto_l_pinky_short_o.attr("stiletto_l_pinky_short"); 
		        console.log("stiletto_l_pinky_short..."+stiletto_l_pinky_short);
		        jQuery('#stiletto_l_pinky_short').val(stiletto_l_pinky_short);
		          //Medium
		        var stiletto_l_pinky_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_pinky_medium = stiletto_l_pinky_medium_o.attr("stiletto_l_pinky_medium"); 
		        jQuery('#stiletto_l_pinky_medium').val(stiletto_l_pinky_medium);
		          //Large
		        var stiletto_l_pinky_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_pinky_large = stiletto_l_pinky_large_o.attr("stiletto_l_pinky_large"); 
		        jQuery('#stiletto_l_pinky_large').val(stiletto_l_pinky_large);
		        
		        //Ring
		          //Short
		        var stiletto_l_ring_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_ring_short = stiletto_l_ring_short_o.attr("stiletto_l_ring_short"); 
		        jQuery('#stiletto_l_ring_short').val(stiletto_l_ring_short);
		          //Medium
		        var stiletto_l_ring_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_ring_medium = stiletto_l_ring_medium_o.attr("stiletto_l_ring_medium"); 
		        jQuery('#stiletto_l_ring_medium').val(stiletto_l_pinky_medium);
		          //Large
		        var stiletto_l_ring_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_ring_large = stiletto_l_ring_short_o.attr("stiletto_l_ring_large"); 
		        jQuery('#stiletto_l_ring_large').val(stiletto_l_ring_large);
		        
		        //Middle
		          //Short
		        var stiletto_l_middle_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_middle_short = stiletto_l_middle_short_o.attr("stiletto_l_middle_short"); 
		        jQuery('#stiletto_l_middle_short').val(stiletto_l_middle_short);
		          //Medium
		        var stiletto_l_middle_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_middle_medium = stiletto_l_pinky_medium_o.attr("stiletto_l_middle_medium"); 
		        jQuery('#stiletto_l_middle_medium').val(stiletto_l_middle_medium);
		          //Large
		        var stiletto_l_middle_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_middle_large = stiletto_l_pinky_short_o.attr("stiletto_l_middle_large"); 
		        jQuery('#stiletto_l_middle_large').val(stiletto_l_middle_large);
		        
		        //Index
		          //Short
		        var stiletto_l_index_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_index_short = stiletto_l_index_short_o.attr("stiletto_l_index_short"); 
		        jQuery('#stiletto_l_index_short').val(stiletto_l_index_short);
		          //Medium
		        var stiletto_l_index_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_index_medium = stiletto_l_index_medium_o.attr("stiletto_l_index_medium"); 
		        jQuery('#stiletto_l_index_medium').val(stiletto_l_index_medium);
		          //Large
		        var stiletto_l_index_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_index_large = stiletto_l_index_large_o.attr("stiletto_l_index_large"); 
		        console.log("stiletto_l_index_large.."+stiletto_l_index_large);
		        jQuery('#stiletto_l_index_large').val(stiletto_l_index_large);
		        
		        //Thumb
		          //Short
		        var stiletto_l_thumb_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_thumb_short = stiletto_l_pinky_short_o.attr("stiletto_l_thumb_short"); 
		        jQuery('#stiletto_l_thumb_short').val(stiletto_l_thumb_short);
		          //Medium
		        var stiletto_l_thumb_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_thumb_medium = stiletto_l_thumb_medium_o.attr("stiletto_l_thumb_medium"); 
		        jQuery('#stiletto_l_thumb_medium').val(stiletto_l_thumb_medium);
		          //Large
		        var stiletto_l_thumb_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_l_thumb_large = stiletto_l_thumb_large_o.attr("stiletto_l_thumb_large"); 
		        jQuery('#stiletto_l_thumb_large').val(stiletto_l_thumb_large); 
		        
		        //Right Hand Stiletto
		        //Pinky
		          //Short
		        var stiletto_r_pinky_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_pinky_short = stiletto_r_pinky_short_o.attr("stiletto_r_pinky_short"); 
		        jQuery('#stiletto_r_pinky_short').val(stiletto_r_pinky_short);
		          //Medium
		        var stiletto_r_pinky_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_pinky_medium = stiletto_r_pinky_medium_o.attr("stiletto_r_pinky_medium"); 
		        jQuery('#stiletto_r_pinky_medium').val(stiletto_r_pinky_medium);
		          //Large
		        var stiletto_r_pinky_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_pinky_large = stiletto_r_pinky_large_o.attr("stiletto_r_pinky_large"); 
		        jQuery('#stiletto_r_pinky_large').val(stiletto_r_pinky_large);
		        
		        //Ring
		          //Short
		        var stiletto_r_ring_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_ring_short = stiletto_r_ring_short_o.attr("stiletto_r_ring_short"); 
		        jQuery('#stiletto_r_ring_short').val(stiletto_r_ring_short);
		          //Medium
		        var stiletto_r_ring_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_ring_medium = stiletto_r_ring_medium_o.attr("stiletto_r_ring_medium"); 
		        jQuery('#stiletto_r_ring_medium').val(stiletto_r_pinky_medium);
		          //Large
		        var stiletto_r_ring_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_ring_large = stiletto_r_ring_short_o.attr("stiletto_r_ring_large"); 
		        jQuery('#stiletto_r_ring_large').val(stiletto_r_ring_large);
		        
		        //Middle
		          //Short
		        var stiletto_r_middle_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_middle_short = stiletto_r_middle_short_o.attr("stiletto_r_middle_short"); 
		        jQuery('#stiletto_r_middle_short').val(stiletto_r_middle_short);
		          //Medium
		        var stiletto_r_middle_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_middle_medium = stiletto_r_pinky_medium_o.attr("stiletto_r_middle_medium"); 
		        jQuery('#stiletto_r_middle_medium').val(stiletto_r_middle_medium);
		          //Large
		        var stiletto_r_middle_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_middle_large = stiletto_r_middle_large_o.attr("stiletto_r_middle_large"); 
		        jQuery('#stiletto_r_middle_large').val(stiletto_r_middle_large);
		        
		        //Index
		          //Short
		        var stiletto_r_index_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_index_short = stiletto_r_index_short_o.attr("stiletto_r_index_short"); 
		        jQuery('#stiletto_r_index_short').val(stiletto_r_index_short);
		          //Medium
		        var stiletto_r_index_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_index_medium = stiletto_r_index_medium_o.attr("stiletto_r_index_medium"); 
		        jQuery('#stiletto_r_index_medium').val(stiletto_r_index_medium);
		          //Large
		        var stiletto_r_index_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_index_large = stiletto_r_index_large_o.attr("stiletto_r_index_large"); 
		        console.log("stiletto_r_index_large.."+stiletto_r_index_large);
		        jQuery('#stiletto_r_index_large').val(stiletto_r_index_large);
		        
		        //Thumb
		          //Short
		        var stiletto_r_thumb_short_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_thumb_short = stiletto_r_thumb_short_o.attr("stiletto_r_thumb_short"); 
		        jQuery('#stiletto_r_thumb_short').val(stiletto_r_thumb_short);
		          //Medium
		        var stiletto_r_thumb_medium_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_thumb_medium = stiletto_r_thumb_medium_o.attr("stiletto_r_thumb_medium"); 
		        jQuery('#stiletto_r_thumb_medium').val(stiletto_r_thumb_medium);
		          //Large
		        var stiletto_r_thumb_large_o = jQuery(this).find('option:selected'); 
		        var stiletto_r_thumb_large = stiletto_r_thumb_large_o.attr("stiletto_r_thumb_large"); 
		        jQuery('#stiletto_r_thumb_large').val(stiletto_r_thumb_large);

		        //Left Hand Oval
				//Pinky
				  //Short
				var oval_l_pinky_short_o = jQuery(this).find('option:selected'); 
				var oval_l_pinky_short = oval_l_pinky_short_o.attr("oval_l_pinky_short"); 
				jQuery('#oval_l_pinky_short').val(oval_l_pinky_short);
				  //Medium
				var oval_l_pinky_medium_o = jQuery(this).find('option:selected'); 
				var oval_l_pinky_medium = oval_l_pinky_medium_o.attr("oval_l_pinky_medium"); 
				jQuery('#oval_l_pinky_medium').val(oval_l_pinky_medium);
				  //Large
				var oval_l_pinky_large_o = jQuery(this).find('option:selected'); 
				var oval_l_pinky_large = oval_l_pinky_large_o.attr("oval_l_pinky_large"); 
				jQuery('#oval_l_pinky_large').val(oval_l_pinky_large);

				//Ring
				  //Short
				var oval_l_ring_short_o = jQuery(this).find('option:selected'); 
				var oval_l_ring_short = oval_l_ring_short_o.attr("oval_l_ring_short"); 
				jQuery('#oval_l_ring_short').val(oval_l_ring_short);
				  //Medium
				var oval_l_ring_medium_o = jQuery(this).find('option:selected'); 
				var oval_l_ring_medium = oval_l_ring_medium_o.attr("oval_l_ring_medium"); 
				jQuery('#oval_l_ring_medium').val(oval_l_pinky_medium);
				  //Large
				var oval_l_ring_large_o = jQuery(this).find('option:selected'); 
				var oval_l_ring_large = oval_l_ring_short_o.attr("oval_l_ring_large"); 
				jQuery('#oval_l_ring_large').val(oval_l_ring_large);

				//Middle
				  //Short
				var oval_l_middle_short_o = jQuery(this).find('option:selected'); 
				var oval_l_middle_short = oval_l_middle_short_o.attr("oval_l_middle_short"); 
				jQuery('#oval_l_middle_short').val(oval_l_middle_short);
				  //Medium
				var oval_l_middle_medium_o = jQuery(this).find('option:selected'); 
				var oval_l_middle_medium = oval_l_pinky_medium_o.attr("oval_l_middle_medium"); 
				jQuery('#oval_l_middle_medium').val(oval_l_middle_medium);
				  //Large
				var oval_l_middle_large_o = jQuery(this).find('option:selected'); 
				var oval_l_middle_large = oval_l_pinky_short_o.attr("oval_l_middle_large"); 
				jQuery('#oval_l_middle_large').val(oval_l_middle_large);

				//Index
				  //Short
				var oval_l_index_short_o = jQuery(this).find('option:selected'); 
				var oval_l_index_short = oval_l_index_short_o.attr("oval_l_index_short"); 
				jQuery('#oval_l_index_short').val(oval_l_index_short);
				  //Medium
				var oval_l_index_medium_o = jQuery(this).find('option:selected'); 
				var oval_l_index_medium = oval_l_index_medium_o.attr("oval_l_index_medium"); 
				jQuery('#oval_l_index_medium').val(oval_l_index_medium);
				  //Large
				var oval_l_index_large_o = jQuery(this).find('option:selected'); 
				var oval_l_index_large = oval_l_index_large_o.attr("oval_l_index_large"); 
				console.log("oval_l_index_large.."+oval_l_index_large);
				jQuery('#oval_l_index_large').val(oval_l_index_large);

				//Thumb
				  //Short
				var oval_l_thumb_short_o = jQuery(this).find('option:selected'); 
				var oval_l_thumb_short = oval_l_pinky_short_o.attr("oval_l_thumb_short"); 
				jQuery('#oval_l_thumb_short').val(oval_l_thumb_short);
				  //Medium
				var oval_l_thumb_medium_o = jQuery(this).find('option:selected'); 
				var oval_l_thumb_medium = oval_l_thumb_medium_o.attr("oval_l_thumb_medium"); 
				jQuery('#oval_l_thumb_medium').val(oval_l_thumb_medium);
				  //Large
				var oval_l_thumb_large_o = jQuery(this).find('option:selected'); 
				var oval_l_thumb_large = oval_l_thumb_large_o.attr("oval_l_thumb_large"); 
				jQuery('#oval_l_thumb_large').val(oval_l_thumb_large); 

				//Right Hand Oval
				//Pinky
				  //Short
				var oval_r_pinky_short_o = jQuery(this).find('option:selected'); 
				var oval_r_pinky_short = oval_r_pinky_short_o.attr("oval_r_pinky_short"); 
				jQuery('#oval_r_pinky_short').val(oval_r_pinky_short);
				  //Medium
				var oval_r_pinky_medium_o = jQuery(this).find('option:selected'); 
				var oval_r_pinky_medium = oval_r_pinky_medium_o.attr("oval_r_pinky_medium"); 
				jQuery('#oval_r_pinky_medium').val(oval_r_pinky_medium);
				  //Large
				var oval_r_pinky_large_o = jQuery(this).find('option:selected'); 
				var oval_r_pinky_large = oval_r_pinky_large_o.attr("oval_r_pinky_large"); 
				jQuery('#oval_r_pinky_large').val(oval_r_pinky_large);

				//Ring
				  //Short
				var oval_r_ring_short_o = jQuery(this).find('option:selected'); 
				var oval_r_ring_short = oval_r_ring_short_o.attr("oval_r_ring_short"); 
				jQuery('#oval_r_ring_short').val(oval_r_ring_short);
				  //Medium
				var oval_r_ring_medium_o = jQuery(this).find('option:selected'); 
				var oval_r_ring_medium = oval_r_ring_medium_o.attr("oval_r_ring_medium"); 
				jQuery('#oval_r_ring_medium').val(oval_r_pinky_medium);
				  //Large
				var oval_r_ring_large_o = jQuery(this).find('option:selected'); 
				var oval_r_ring_large = oval_r_ring_short_o.attr("oval_r_ring_large"); 
				jQuery('#oval_r_ring_large').val(oval_r_ring_large);

				//Middle
				  //Short
				var oval_r_middle_short_o = jQuery(this).find('option:selected'); 
				var oval_r_middle_short = oval_r_middle_short_o.attr("oval_r_middle_short"); 
				jQuery('#oval_r_middle_short').val(oval_r_middle_short);
				  //Medium
				var oval_r_middle_medium_o = jQuery(this).find('option:selected'); 
				var oval_r_middle_medium = oval_r_pinky_medium_o.attr("oval_r_middle_medium"); 
				jQuery('#oval_r_middle_medium').val(oval_r_middle_medium);
				  //Large
				var oval_r_middle_large_o = jQuery(this).find('option:selected'); 
				var oval_r_middle_large = oval_r_middle_large_o.attr("oval_r_middle_large"); 
				jQuery('#oval_r_middle_large').val(oval_r_middle_large);

				//Index
				  //Short
				var oval_r_index_short_o = jQuery(this).find('option:selected'); 
				var oval_r_index_short = oval_r_index_short_o.attr("oval_r_index_short"); 
				jQuery('#oval_r_index_short').val(oval_r_index_short);
				  //Medium
				var oval_r_index_medium_o = jQuery(this).find('option:selected'); 
				var oval_r_index_medium = oval_r_index_medium_o.attr("oval_r_index_medium"); 
				jQuery('#oval_r_index_medium').val(oval_r_index_medium);
				  //Large
				var oval_r_index_large_o = jQuery(this).find('option:selected'); 
				var oval_r_index_large = oval_r_index_large_o.attr("oval_r_index_large"); 
				jQuery('#oval_r_index_large').val(oval_r_index_large);

				//Thumb
				  //Short
				var oval_r_thumb_short_o = jQuery(this).find('option:selected'); 
				var oval_r_thumb_short = oval_r_thumb_short_o.attr("oval_r_thumb_short"); 
				jQuery('#oval_r_thumb_short').val(oval_r_thumb_short);
				  //Medium
				var oval_r_thumb_medium_o = jQuery(this).find('option:selected'); 
				var oval_r_thumb_medium = oval_r_thumb_medium_o.attr("oval_r_thumb_medium"); 
				jQuery('#oval_r_thumb_medium').val(oval_r_thumb_medium);
				  //Large
				var oval_r_thumb_large_o = jQuery(this).find('option:selected'); 
				var oval_r_thumb_large = oval_r_thumb_large_o.attr("oval_r_thumb_large"); 
				jQuery('#oval_r_thumb_large').val(oval_r_thumb_large);
		        
			    //Left Hand Coffin
				//Pinky
				  //Short
				var coffin_l_pinky_short_o = jQuery(this).find('option:selected'); 
				var coffin_l_pinky_short = coffin_l_pinky_short_o.attr("coffin_l_pinky_short"); 
				jQuery('#coffin_l_pinky_short').val(coffin_l_pinky_short);
				  //Medium
				var coffin_l_pinky_medium_o = jQuery(this).find('option:selected'); 
				var coffin_l_pinky_medium = coffin_l_pinky_medium_o.attr("coffin_l_pinky_medium"); 
				jQuery('#coffin_l_pinky_medium').val(coffin_l_pinky_medium);
				  //Large
				var coffin_l_pinky_large_o = jQuery(this).find('option:selected'); 
				var coffin_l_pinky_large = coffin_l_pinky_large_o.attr("coffin_l_pinky_large"); 
				jQuery('#coffin_l_pinky_large').val(coffin_l_pinky_large);

				//Ring
				  //Short
				var coffin_l_ring_short_o = jQuery(this).find('option:selected'); 
				var coffin_l_ring_short = coffin_l_ring_short_o.attr("coffin_l_ring_short"); 
				jQuery('#coffin_l_ring_short').val(coffin_l_ring_short);
				  //Medium
				var coffin_l_ring_medium_o = jQuery(this).find('option:selected'); 
				var coffin_l_ring_medium = coffin_l_ring_medium_o.attr("coffin_l_ring_medium"); 
				jQuery('#coffin_l_ring_medium').val(coffin_l_pinky_medium);
				  //Large
				var coffin_l_ring_large_o = jQuery(this).find('option:selected'); 
				var coffin_l_ring_large = coffin_l_ring_short_o.attr("coffin_l_ring_large"); 
				jQuery('#coffin_l_ring_large').val(coffin_l_ring_large);

				//Middle
				  //Short
				var coffin_l_middle_short_o = jQuery(this).find('option:selected'); 
				var coffin_l_middle_short = coffin_l_middle_short_o.attr("coffin_l_middle_short"); 
				jQuery('#coffin_l_middle_short').val(coffin_l_middle_short);
				  //Medium
				var coffin_l_middle_medium_o = jQuery(this).find('option:selected'); 
				var coffin_l_middle_medium = coffin_l_pinky_medium_o.attr("coffin_l_middle_medium"); 
				jQuery('#coffin_l_middle_medium').val(coffin_l_middle_medium);
				  //Large
				var coffin_l_middle_large_o = jQuery(this).find('option:selected'); 
				var coffin_l_middle_large = coffin_l_pinky_short_o.attr("coffin_l_middle_large"); 
				jQuery('#coffin_l_middle_large').val(coffin_l_middle_large);

				//Index
				  //Short
				var coffin_l_index_short_o = jQuery(this).find('option:selected'); 
				var coffin_l_index_short = coffin_l_index_short_o.attr("coffin_l_index_short"); 
				jQuery('#coffin_l_index_short').val(coffin_l_index_short);
				  //Medium
				var coffin_l_index_medium_o = jQuery(this).find('option:selected'); 
				var coffin_l_index_medium = coffin_l_index_medium_o.attr("coffin_l_index_medium"); 
				jQuery('#coffin_l_index_medium').val(coffin_l_index_medium);
				  //Large
				var coffin_l_index_large_o = jQuery(this).find('option:selected'); 
				var coffin_l_index_large = coffin_l_index_large_o.attr("coffin_l_index_large"); 
				console.log("coffin_l_index_large.."+coffin_l_index_large);
				jQuery('#coffin_l_index_large').val(coffin_l_index_large);

				//Thumb
				  //Short
				var coffin_l_thumb_short_o = jQuery(this).find('option:selected'); 
				var coffin_l_thumb_short = coffin_l_pinky_short_o.attr("coffin_l_thumb_short"); 
				jQuery('#coffin_l_thumb_short').val(coffin_l_thumb_short);
				  //Medium
				var coffin_l_thumb_medium_o = jQuery(this).find('option:selected'); 
				var coffin_l_thumb_medium = coffin_l_thumb_medium_o.attr("coffin_l_thumb_medium"); 
				jQuery('#coffin_l_thumb_medium').val(coffin_l_thumb_medium);
				  //Large
				var coffin_l_thumb_large_o = jQuery(this).find('option:selected'); 
				var coffin_l_thumb_large = coffin_l_thumb_large_o.attr("coffin_l_thumb_large"); 
				jQuery('#coffin_l_thumb_large').val(coffin_l_thumb_large); 

				//Right Hand Coffin
				//Pinky
				  //Short
				var coffin_r_pinky_short_o = jQuery(this).find('option:selected'); 
				var coffin_r_pinky_short = coffin_r_pinky_short_o.attr("coffin_r_pinky_short"); 
				jQuery('#coffin_r_pinky_short').val(coffin_r_pinky_short);
				  //Medium
				var coffin_r_pinky_medium_o = jQuery(this).find('option:selected'); 
				var coffin_r_pinky_medium = coffin_r_pinky_medium_o.attr("coffin_r_pinky_medium"); 
				jQuery('#coffin_r_pinky_medium').val(coffin_r_pinky_medium);
				  //Large
				var coffin_r_pinky_large_o = jQuery(this).find('option:selected'); 
				var coffin_r_pinky_large = coffin_r_pinky_large_o.attr("coffin_r_pinky_large"); 
				jQuery('#coffin_r_pinky_large').val(coffin_r_pinky_large);

				//Ring
				  //Short
				var coffin_r_ring_short_o = jQuery(this).find('option:selected'); 
				var coffin_r_ring_short = coffin_r_ring_short_o.attr("coffin_r_ring_short"); 
				jQuery('#coffin_r_ring_short').val(coffin_r_ring_short);
				  //Medium
				var coffin_r_ring_medium_o = jQuery(this).find('option:selected'); 
				var coffin_r_ring_medium = coffin_r_ring_medium_o.attr("coffin_r_ring_medium"); 
				jQuery('#coffin_r_ring_medium').val(coffin_r_pinky_medium);
				  //Large
				var coffin_r_ring_large_o = jQuery(this).find('option:selected'); 
				var coffin_r_ring_large = coffin_r_ring_short_o.attr("coffin_r_ring_large"); 
				jQuery('#coffin_r_ring_large').val(coffin_r_ring_large);

				//Middle
				  //Short
				var coffin_r_middle_short_o = jQuery(this).find('option:selected'); 
				var coffin_r_middle_short = coffin_r_middle_short_o.attr("coffin_r_middle_short"); 
				jQuery('#coffin_r_middle_short').val(coffin_r_middle_short);
				  //Medium
				var coffin_r_middle_medium_o = jQuery(this).find('option:selected'); 
				var coffin_r_middle_medium = coffin_r_pinky_medium_o.attr("coffin_r_middle_medium"); 
				jQuery('#coffin_r_middle_medium').val(coffin_r_middle_medium);
				  //Large
				var coffin_r_middle_large_o = jQuery(this).find('option:selected'); 
				var coffin_r_middle_large = coffin_r_middle_large_o.attr("coffin_r_middle_large"); 
				jQuery('#coffin_r_middle_large').val(coffin_r_middle_large);

				//Index
				  //Short
				var coffin_r_index_short_o = jQuery(this).find('option:selected'); 
				var coffin_r_index_short = coffin_r_index_short_o.attr("coffin_r_index_short"); 
				jQuery('#coffin_r_index_short').val(coffin_r_index_short);
				  //Medium
				var coffin_r_index_medium_o = jQuery(this).find('option:selected'); 
				var coffin_r_index_medium = coffin_r_index_medium_o.attr("coffin_r_index_medium"); 
				jQuery('#coffin_r_index_medium').val(coffin_r_index_medium);
				  //Large
				var coffin_r_index_large_o = jQuery(this).find('option:selected'); 
				var coffin_r_index_large = coffin_r_index_large_o.attr("coffin_r_index_large"); 
				jQuery('#coffin_r_index_large').val(coffin_r_index_large);

				//Thumb
				  //Short
				var coffin_r_thumb_short_o = jQuery(this).find('option:selected'); 
				var coffin_r_thumb_short = coffin_r_thumb_short_o.attr("coffin_r_thumb_short"); 
				jQuery('#coffin_r_thumb_short').val(coffin_r_thumb_short);
				  //Medium
				var coffin_r_thumb_medium_o = jQuery(this).find('option:selected'); 
				var coffin_r_thumb_medium = coffin_r_thumb_medium_o.attr("coffin_r_thumb_medium"); 
				jQuery('#coffin_r_thumb_medium').val(coffin_r_thumb_medium);
				  //Large
				var coffin_r_thumb_large_o = jQuery(this).find('option:selected'); 
				var coffin_r_thumb_large = coffin_r_thumb_large_o.attr("coffin_r_thumb_large"); 
				jQuery('#coffin_r_thumb_large').val(coffin_r_thumb_large);
    
			    //Left Hand Square
				//Pinky
				  //Short
				var square_l_pinky_short_o = jQuery(this).find('option:selected'); 
				var square_l_pinky_short = square_l_pinky_short_o.attr("square_l_pinky_short"); 
				jQuery('#square_l_pinky_short').val(square_l_pinky_short);
				  //Medium
				var square_l_pinky_medium_o = jQuery(this).find('option:selected'); 
				var square_l_pinky_medium = square_l_pinky_medium_o.attr("square_l_pinky_medium"); 
				jQuery('#square_l_pinky_medium').val(square_l_pinky_medium);
				  //Large
				var square_l_pinky_large_o = jQuery(this).find('option:selected'); 
				var square_l_pinky_large = square_l_pinky_large_o.attr("square_l_pinky_large"); 
				jQuery('#square_l_pinky_large').val(square_l_pinky_large);

				//Ring
				  //Short
				var square_l_ring_short_o = jQuery(this).find('option:selected'); 
				var square_l_ring_short = square_l_ring_short_o.attr("square_l_ring_short"); 
				jQuery('#square_l_ring_short').val(square_l_ring_short);
				  //Medium
				var square_l_ring_medium_o = jQuery(this).find('option:selected'); 
				var square_l_ring_medium = square_l_ring_medium_o.attr("square_l_ring_medium"); 
				jQuery('#square_l_ring_medium').val(square_l_pinky_medium);
				  //Large
				var square_l_ring_large_o = jQuery(this).find('option:selected'); 
				var square_l_ring_large = square_l_ring_short_o.attr("square_l_ring_large"); 
				jQuery('#square_l_ring_large').val(square_l_ring_large);

				//Middle
				  //Short
				var square_l_middle_short_o = jQuery(this).find('option:selected'); 
				var square_l_middle_short = square_l_middle_short_o.attr("square_l_middle_short"); 
				jQuery('#square_l_middle_short').val(square_l_middle_short);
				  //Medium
				var square_l_middle_medium_o = jQuery(this).find('option:selected'); 
				var square_l_middle_medium = square_l_pinky_medium_o.attr("square_l_middle_medium"); 
				jQuery('#square_l_middle_medium').val(square_l_middle_medium);
				  //Large
				var square_l_middle_large_o = jQuery(this).find('option:selected'); 
				var square_l_middle_large = square_l_pinky_short_o.attr("square_l_middle_large"); 
				jQuery('#square_l_middle_large').val(square_l_middle_large);

				//Index
				  //Short
				var square_l_index_short_o = jQuery(this).find('option:selected'); 
				var square_l_index_short = square_l_index_short_o.attr("square_l_index_short"); 
				jQuery('#square_l_index_short').val(square_l_index_short);
				  //Medium
				var square_l_index_medium_o = jQuery(this).find('option:selected'); 
				var square_l_index_medium = square_l_index_medium_o.attr("square_l_index_medium"); 
				jQuery('#square_l_index_medium').val(square_l_index_medium);
				  //Large
				var square_l_index_large_o = jQuery(this).find('option:selected'); 
				var square_l_index_large = square_l_index_large_o.attr("square_l_index_large"); 
				console.log("square_l_index_large.."+square_l_index_large);
				jQuery('#square_l_index_large').val(square_l_index_large);

				//Thumb
				  //Short
				var square_l_thumb_short_o = jQuery(this).find('option:selected'); 
				var square_l_thumb_short = square_l_thumb_short_o.attr("square_l_thumb_short"); 
				console.log(square_l_thumb_short);
				jQuery('#square_l_thumb_short').val(square_l_thumb_short);
				  //Medium
				var square_l_thumb_medium_o = jQuery(this).find('option:selected'); 
				var square_l_thumb_medium = square_l_thumb_medium_o.attr("square_l_thumb_medium"); 
				jQuery('#square_l_thumb_medium').val(square_l_thumb_medium);
				  //Large
				var square_l_thumb_large_o = jQuery(this).find('option:selected'); 
				var square_l_thumb_large = square_l_thumb_large_o.attr("square_l_thumb_large"); 
				jQuery('#square_l_thumb_large').val(square_l_thumb_large); 

				//Right Hand Square
				//Pinky
				  //Short
				var square_r_pinky_short_o = jQuery(this).find('option:selected'); 
				var square_r_pinky_short = square_r_pinky_short_o.attr("square_r_pinky_short"); 
				jQuery('#square_r_pinky_short').val(square_r_pinky_short);
				  //Medium
				var square_r_pinky_medium_o = jQuery(this).find('option:selected'); 
				var square_r_pinky_medium = square_r_pinky_medium_o.attr("square_r_pinky_medium"); 
				jQuery('#square_r_pinky_medium').val(square_r_pinky_medium);
				  //Large
				var square_r_pinky_large_o = jQuery(this).find('option:selected'); 
				var square_r_pinky_large = square_r_pinky_large_o.attr("square_r_pinky_large"); 
				jQuery('#square_r_pinky_large').val(square_r_pinky_large);

				//Ring
				  //Short
				var square_r_ring_short_o = jQuery(this).find('option:selected'); 
				var square_r_ring_short = square_r_ring_short_o.attr("square_r_ring_short"); 
				jQuery('#square_r_ring_short').val(square_r_ring_short);
				  //Medium
				var square_r_ring_medium_o = jQuery(this).find('option:selected'); 
				var square_r_ring_medium = square_r_ring_medium_o.attr("square_r_ring_medium"); 
				jQuery('#square_r_ring_medium').val(square_r_pinky_medium);
				  //Large
				var square_r_ring_large_o = jQuery(this).find('option:selected'); 
				var square_r_ring_large = square_r_ring_short_o.attr("square_r_ring_large"); 
				jQuery('#square_r_ring_large').val(square_r_ring_large);

				//Middle
				  //Short
				var square_r_middle_short_o = jQuery(this).find('option:selected'); 
				var square_r_middle_short = square_r_middle_short_o.attr("square_r_middle_short"); 
				jQuery('#square_r_middle_short').val(square_r_middle_short);
				  //Medium
				var square_r_middle_medium_o = jQuery(this).find('option:selected'); 
				var square_r_middle_medium = square_r_pinky_medium_o.attr("square_r_middle_medium"); 
				jQuery('#square_r_middle_medium').val(square_r_middle_medium);
				  //Large
				var square_r_middle_large_o = jQuery(this).find('option:selected'); 
				var square_r_middle_large = square_r_middle_large_o.attr("square_r_middle_large"); 
				jQuery('#square_r_middle_large').val(square_r_middle_large);

				//Index
				  //Short
				var square_r_index_short_o = jQuery(this).find('option:selected'); 
				var square_r_index_short = square_r_index_short_o.attr("square_r_index_short"); 
				jQuery('#square_r_index_short').val(square_r_index_short);
				  //Medium
				var square_r_index_medium_o = jQuery(this).find('option:selected'); 
				var square_r_index_medium = square_r_index_medium_o.attr("square_r_index_medium"); 
				jQuery('#square_r_index_medium').val(square_r_index_medium);
				  //Large
				var square_r_index_large_o = jQuery(this).find('option:selected'); 
				var square_r_index_large = square_r_index_large_o.attr("square_r_index_large"); 
				jQuery('#square_r_index_large').val(square_r_index_large);

				//Thumb
				  //Short
				var square_r_thumb_short_o = jQuery(this).find('option:selected'); 
				var square_r_thumb_short = square_r_thumb_short_o.attr("square_r_thumb_short"); 
				jQuery('#square_r_thumb_short').val(square_r_thumb_short);
				  //Medium
				var square_r_thumb_medium_o = jQuery(this).find('option:selected'); 
				var square_r_thumb_medium = square_r_thumb_medium_o.attr("square_r_thumb_medium"); 
				jQuery('#square_r_thumb_medium').val(square_r_thumb_medium);
				  //Large
				var square_r_thumb_large_o = jQuery(this).find('option:selected'); 
				var square_r_thumb_large = square_r_thumb_large_o.attr("square_r_thumb_large"); 
				jQuery('#square_r_thumb_large').val(square_r_thumb_large);				
		    }); 
    	});
    </script>
    <div class="elementor-container elementor-column-gap-no nails-meaurement-single-product">
        
    	<div class="elementor-row">
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Stiletto Left Hand -->
				<input type="text" id="stiletto_l_pinky_short" name="stiletto_l_pinky_short">
				<input type="text" id="stiletto_l_pinky_medium" name="stiletto_l_pinky_medium">
				<input type="text" id="stiletto_l_pinky_large" name="stiletto_l_pinky_large">

				<input type="text" id="stiletto_l_ring_short" name="stiletto_l_ring_short">
				<input type="text" id="stiletto_l_ring_medium" name="stiletto_l_ring_medium">
				<input type="text" id="stiletto_l_ring_large" name="stiletto_l_ring_large">

				<input type="text" id="stiletto_l_middle_short" name="stiletto_l_middle_short">
				<input type="text" id="stiletto_l_middle_medium" name="stiletto_l_middle_medium">
				<input type="text" id="stiletto_l_middle_large" name="stiletto_l_middle_large">

				<input type="text" id="stiletto_l_index_short" name="stiletto_l_index_short">
				<input type="text" id="stiletto_l_index_medium" name="stiletto_l_index_medium">
				<input type="text" id="stiletto_l_index_large" name="stiletto_l_index_large">

				<input type="text" id="stiletto_l_thumb_short" name="stiletto_l_thumb_short">
				<input type="text" id="stiletto_l_thumb_medium" name="stiletto_l_thumb_medium">
				<input type="text" id="stiletto_l_thumb_large" name="stiletto_l_thumb_large">
			</div>
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Stiletto Right Hand -->
				<input type="text" id="stiletto_r_pinky_short" name="stiletto_r_pinky_short">
				<input type="text" id="stiletto_r_pinky_medium" name="stiletto_r_pinky_medium">
				<input type="text" id="stiletto_r_pinky_large" name="stiletto_r_pinky_large">

				<input type="text" id="stiletto_r_ring_short" name="stiletto_r_ring_short">
				<input type="text" id="stiletto_r_ring_medium" name="stiletto_r_ring_medium">
				<input type="text" id="stiletto_r_ring_large" name="stiletto_r_ring_large">

				<input type="text" id="stiletto_r_middle_short" name="stiletto_r_middle_short">
				<input type="text" id="stiletto_r_middle_medium" name="stiletto_r_middle_medium">
				<input type="text" id="stiletto_r_middle_large" name="stiletto_r_middle_large">

				<input type="text" id="stiletto_r_index_short" name="stiletto_r_index_short">
				<input type="text" id="stiletto_r_index_medium" name="stiletto_r_index_medium">
				<input type="text" id="stiletto_r_index_large" name="stiletto_r_index_large">

				<input type="text" id="stiletto_r_thumb_short" name="stiletto_r_thumb_short">
				<input type="text" id="stiletto_r_thumb_medium" name="stiletto_r_thumb_medium">
				<input type="text" id="stiletto_r_thumb_large" name="stiletto_r_thumb_large">
			</div>
    	</div>
    	<div class="elementor-row">
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Oval Left Hand -->
				<input type="text" id="oval_l_pinky_short" name="oval_l_pinky_short">
				<input type="text" id="oval_l_pinky_medium" name="oval_l_pinky_medium">
				<input type="text" id="oval_l_pinky_large" name="oval_l_pinky_large">

				<input type="text" id="oval_l_ring_short" name="oval_l_ring_short">
				<input type="text" id="oval_l_ring_medium" name="oval_l_ring_medium">
				<input type="text" id="oval_l_ring_large" name="oval_l_ring_large">

				<input type="text" id="oval_l_middle_short" name="oval_l_middle_short">
				<input type="text" id="oval_l_middle_medium" name="oval_l_middle_medium">
				<input type="text" id="oval_l_middle_large" name="oval_l_middle_large">

				<input type="text" id="oval_l_index_short" name="oval_l_index_short">
				<input type="text" id="oval_l_index_medium" name="oval_l_index_medium">
				<input type="text" id="oval_l_index_large" name="oval_l_index_large">

				<input type="text" id="oval_l_thumb_short" name="oval_l_thumb_short">
				<input type="text" id="oval_l_thumb_medium" name="oval_l_thumb_medium">
				<input type="text" id="oval_l_thumb_large" name="oval_l_thumb_large">
			</div>
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Oval Right Hand -->
				<input type="text" id="oval_r_pinky_short" name="oval_r_pinky_short">
				<input type="text" id="oval_r_pinky_medium" name="oval_r_pinky_medium">
				<input type="text" id="oval_r_pinky_large" name="oval_r_pinky_large">

				<input type="text" id="oval_r_ring_short" name="oval_r_ring_short">
				<input type="text" id="oval_r_ring_medium" name="oval_r_ring_medium">
				<input type="text" id="oval_r_ring_large" name="oval_r_ring_large">

				<input type="text" id="oval_r_middle_short" name="oval_r_middle_short">
				<input type="text" id="oval_r_middle_medium" name="oval_r_middle_medium">
				<input type="text" id="oval_r_middle_large" name="oval_r_middle_large">

				<input type="text" id="oval_r_index_short" name="oval_r_index_short">
				<input type="text" id="oval_r_index_medium" name="oval_r_index_medium">
				<input type="text" id="oval_r_index_large" name="oval_r_index_large">

				<input type="text" id="oval_r_thumb_short" name="oval_r_thumb_short">
				<input type="text" id="oval_r_thumb_medium" name="oval_r_thumb_medium">
				<input type="text" id="oval_r_thumb_large" name="oval_r_thumb_large">
			</div>
    	</div>
    	<div class="elementor-row">
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Coffin Left Hand -->
				<input type="text" id="coffin_l_pinky_short" name="coffin_l_pinky_short">
				<input type="text" id="coffin_l_pinky_medium" name="coffin_l_pinky_medium">
				<input type="text" id="coffin_l_pinky_large" name="coffin_l_pinky_large">

				<input type="text" id="coffin_l_ring_short" name="coffin_l_ring_short">
				<input type="text" id="coffin_l_ring_medium" name="coffin_l_ring_medium">
				<input type="text" id="coffin_l_ring_large" name="coffin_l_ring_large">

				<input type="text" id="coffin_l_middle_short" name="coffin_l_middle_short">
				<input type="text" id="coffin_l_middle_medium" name="coffin_l_middle_medium">
				<input type="text" id="coffin_l_middle_large" name="coffin_l_middle_large">

				<input type="text" id="coffin_l_index_short" name="coffin_l_index_short">
				<input type="text" id="coffin_l_index_medium" name="coffin_l_index_medium">
				<input type="text" id="coffin_l_index_large" name="coffin_l_index_large">

				<input type="text" id="coffin_l_thumb_short" name="coffin_l_thumb_short">
				<input type="text" id="coffin_l_thumb_medium" name="coffin_l_thumb_medium">
				<input type="text" id="coffin_l_thumb_large" name="coffin_l_thumb_large">
			</div>
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Coffin Right Hand -->
				<input type="text" id="coffin_r_pinky_short" name="coffin_r_pinky_short">
				<input type="text" id="coffin_r_pinky_medium" name="coffin_r_pinky_medium">
				<input type="text" id="coffin_r_pinky_large" name="coffin_r_pinky_large">

				<input type="text" id="coffin_r_ring_short" name="coffin_r_ring_short">
				<input type="text" id="coffin_r_ring_medium" name="coffin_r_ring_medium">
				<input type="text" id="coffin_r_ring_large" name="coffin_r_ring_large">

				<input type="text" id="coffin_r_middle_short" name="coffin_r_middle_short">
				<input type="text" id="coffin_r_middle_medium" name="coffin_r_middle_medium">
				<input type="text" id="coffin_r_middle_large" name="coffin_r_middle_large">

				<input type="text" id="coffin_r_index_short" name="coffin_r_index_short">
				<input type="text" id="coffin_r_index_medium" name="coffin_r_index_medium">
				<input type="text" id="coffin_r_index_large" name="coffin_r_index_large">

				<input type="text" id="coffin_r_thumb_short" name="coffin_r_thumb_short">
				<input type="text" id="coffin_r_thumb_medium" name="coffin_r_thumb_medium">
				<input type="text" id="coffin_r_thumb_large" name="coffin_r_thumb_large">
			</div>
		</div>
		<div class="elementor-row">
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Square Left Hand -->
				<input type="text" id="square_l_pinky_short" name="square_l_pinky_short">
				<input type="text" id="square_l_pinky_medium" name="square_l_pinky_medium">
				<input type="text" id="square_l_pinky_large" name="square_l_pinky_large">

				<input type="text" id="square_l_ring_short" name="square_l_ring_short">
				<input type="text" id="square_l_ring_medium" name="square_l_ring_medium">
				<input type="text" id="square_l_ring_large" name="square_l_ring_large">

				<input type="text" id="square_l_middle_short" name="square_l_middle_short">
				<input type="text" id="square_l_middle_medium" name="square_l_middle_medium">
				<input type="text" id="square_l_middle_large" name="square_l_middle_large">

				<input type="text" id="square_l_index_short" name="square_l_index_short">
				<input type="text" id="square_l_index_medium" name="square_l_index_medium">
				<input type="text" id="square_l_index_large" name="square_l_index_large">

				<input type="text" id="square_l_thumb_short" name="square_l_thumb_short">
				<input type="text" id="square_l_thumb_medium" name="square_l_thumb_medium">
				<input type="text" id="square_l_thumb_large" name="square_l_thumb_large">
			</div>
			<div class="elementor-column elementor-top-column elementor-element" data-element_type="column">
				<!-- Square Right Hand -->
				<input type="text" id="square_r_pinky_short" name="square_r_pinky_short">
				<input type="text" id="square_r_pinky_medium" name="square_r_pinky_medium">
				<input type="text" id="square_r_pinky_large" name="square_r_pinky_large">

				<input type="text" id="square_r_ring_short" name="square_r_ring_short">
				<input type="text" id="square_r_ring_medium" name="square_r_ring_medium">
				<input type="text" id="square_r_ring_large" name="square_r_ring_large">

				<input type="text" id="square_r_middle_short" name="square_r_middle_short">
				<input type="text" id="square_r_middle_medium" name="square_r_middle_medium">
				<input type="text" id="square_r_middle_large" name="square_r_middle_large">

				<input type="text" id="square_r_index_short" name="square_r_index_short">
				<input type="text" id="square_r_index_medium" name="square_r_index_medium">
				<input type="text" id="square_r_index_large" name="square_r_index_large">

				<input type="text" id="square_r_thumb_short" name="square_r_thumb_short">
				<input type="text" id="square_r_thumb_medium" name="square_r_thumb_medium">
				<input type="text" id="square_r_thumb_large" name="square_r_thumb_large">
			</div>
		</div>
        
    </div>
    <?php
}
add_action( 'woocommerce_before_add_to_cart_button', 'iconic_output_engraving_field', 10 );

/**
 * Add engraving text to cart item.
 * @param array $cart_item_data
 * @param int   $product_id
 * @param int   $variation_id
 * @return array
 */
function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
    $stiletto_l_pinky_short = filter_input( INPUT_POST, 'stiletto_l_pinky_short' );
    if ( empty( $stiletto_l_pinky_short ) ) {
        return $cart_item_data;
    }
    $cart_item_data['stiletto_l_pinky_short'] = $stiletto_l_pinky_short;
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );
